/**
 * 消息提示封装
 */

import Vue from 'vue'
let vue = new Vue()

export const success = (msg = '操作成功') => {
  vue.$message({
    type: 'success',
    message: msg
  })
}

export const info = (msg = '取消操作') => {
  vue.$message({
    type: 'info',
    message: msg
  })
}

export const warn = (msg = '操作失败，请稍后再试') => {
  vue.$message({
    type: 'warning',
    message: msg
  })
}

export const error = (msg = '操作失败，请稍后再试') => {
  vue.$message({
    type: 'error',
    message: msg
  })
}

export const warnNotify = (msg = '操作失败,请稍后再试') => {
  vue.$notify({
    title: '提示',
    message: msg,
    type: 'warning'
  });
}

export const errorNotify = (msg = '操作失败,请稍后再试') => {
  vue.$notify({
    title: '提示',
    message: msg,
    type: 'error'
  });
}

export const successNotify = (msg = '操作成功') => {
  vue.$notify({
    title: '提示',
    message: msg,
    type: 'success'
  });
}

export const infoNotify = (msg = '操作成功') => {
  vue.$notify({
    title: '提示',
    message: msg,
    type: 'info'
  });
}

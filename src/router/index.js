import Vue from 'vue'
import Router from 'vue-router'
import Index from '../pages/Index.vue'
import Login from '../pages/Login.vue'
import Usercenter from '../pages/Usercenter.vue'
import ArticleDetail from '../pages/ArticleDetail.vue'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Index',
      component: Index
    },
    {
      path: '/login',
      name: 'Login',
      component: Login
    },
    {
      path: '/usercenter',
      name: 'Usercenter',
      component: Usercenter
    },
    {
      path: '/article/detail',
      name: 'ArticleDetail',
      component: ArticleDetail
    }
  ]
})

import Vue from 'vue'
import Vuex from 'vuex'
import { setCookie, getCookie, delCookie } from '../utils/util.js'

Vue.use(Vuex)

export default new Vuex.Store({

  state: {
    isLogin: false
  },

  mutations: {
    login(state,userInfo){
      state.isLogin = true
    },
    logout(state){
      // 清空用户信息缓存
      delCookie('authorization')
      localStorage.removeItem('userInfo')
      delCookie('isLogin')
      delCookie('userName')
      delCookie('userPassword')
      delCookie('userPhone')
    }
  }

})

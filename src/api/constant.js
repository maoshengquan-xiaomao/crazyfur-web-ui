/**
 * 常量
 */
const constant = {
  base:{
    imgServer: 'https://www.crazyfur.cn',
	  uploadImgServer: '/api/admin/common/multiUpload',
    loginUrl: 'https://www.crazyfur.cn/#/login',
    indexUrl: 'https://www.crazyfur.cn/#/',
  },
  user: {
    login: '/auth/user/login/', // 密码登录
    phoneLogin: '/auth/user/phone/login/',//手机登录
    register: '/auth/user/register', // 用户注册
    isLogin: '/auth/user/isLogin/', // 查询登录状态
    userInfo: '/auth/user/userInfo/', // 查询用户信息
    userUpdate: '/auth/user/update/',// 更新用户信息
    logout: '/auth/user/logout',//注销登录
    userInfoById: '/auth/user/find/', // 获取用户信息
    updateUserPassword: '/auth/user/password/',// 修改用户密码
    code: 'https://www.crazyfur.cn/api/auth/user/verifyCode', // 生成验证码
    smsSend: '/auth/user/sms/send', // 发送短信验证码
  },
  admin: {
    articleNewest: '/admin/article/newest',// 最新课程
    articleDetail: '/admin/article/detail/',// 文章详情
  },
  pay: {
    alipay: {
      returnUrl: '/third/alipay/returnUrl/',
      notifyUrl: '/third/alipay/notifyUrl/',
      orderAliPay: '/admin/order/alipay'
    },
    tencentpay:{
      orderWeiXinPay: '/admin/order/weixin',// 微信下单
      orderQueryWeiXinPay: '/admin/order/weixin/',// 查询微信订单状态
    }
  },
  other: {
    tokenKey: 'Authorization'
  }
}

export default constant

//1.引入vue
import Vue from 'vue'
//2.新创建一个vue实例
let vue = new Vue()
import axios from 'axios'
import {getCookie} from '../utils/util.js'
import constant from './constant.js'

axios.defaults.baseURL = '/api'
axios.defaults.timeout = 10000
axios.defaults.headers.post['Content-Type'] = 'application/json'

axios.interceptors.request.use(config => {
  config.headers = {
    'Authorization': getCookie('authorization'),
    'Content-Type': 'application/json',
  }
  return config;
}, function(error) {
  return Promise.reject(error)
});

axios.interceptors.response.use(response => {
  return checkStatus(response)
}, function(error) {
  return Promise.reject(error)
});

/**
 * 检查HTTP状态
 * @param {Object} response
 */
function checkStatus(response) {
  if (response && (response.status === 200 || response.status === 304 || response.status === 400)) {
    return response;
  }
  if (response && (response.status === 404)){
    vue.$message.error('服务器资源未找到');
  }
  // 异常状态下，把错误信息返回去
  return {
    status: -404,
    msg: '网络异常'
  }
}

/**
 * 检查结果状态码
 * @param {Object} res
 */
function checkCode(res) {
  console.log(res)
  if (res.code == 'REQUEST_NO_AUTHORIZATION' || res.code == 'USER_NO_LOGIN' || res.code == 'USER_TOKEN_EXPIRE') {
    window.location.replace(constant.base.loginUrl)
  }
}

/**
 * get请求
 */
export const get = (url,params = {}) => {
  return new Promise((resolve,reject) => {
    axios.get(url,{
      params: params
    }).then((response) => {
      checkCode(response.data)
      resolve(response.data);
    }).catch(err => {
      vue.$message.error('网络异常~~~get')
    })
  })
}

/**
 * post请求
 */
export const post = (url,params = {}) => {
  return new Promise((resolve,reject) => {
    axios.post(url,params).then((response) => {
      checkCode(response.data)
      resolve(response.data);
    }).catch(err => {
      vue.$message.error('网络异常~~~post')
    })
  })
}

/**
 * put请求
 */
export const put = (url,params = {}) => {
  return new Promise((resolve,reject) => {
    axios.put(url,params).then((response) => {
      checkCode(response.data)
      resolve(response.data);
    }).catch(err => {
      vue.$message.error('网络异常~~~put')
    })
  })
}

/**
 * delete请求
 */
export const del = (url,params = {}) => {
  return new Promise((resolve,reject) => {
    axios.delete(url,{
      params: params
    }).then((response) => {
      checkCode(response.data)
      resolve(response.data);
    }).catch(err => {
      vue.$message.error('网络异常~~~delete')
    })
  })
}

/**
 * 通用请求
 */
export const http = (url,method = 'get',params = {}) => {
  return new Promise((resolve,reject) => {
    axios({
        method: method,
        url: url,
        data: params
    }).then((response)=>{
      checkCode(response.data)
      resolve(response.data);
    }).catch(err => {
      vue.$message.error('网络异常~~~')
    })
  })
}
